-- Data export script for DCS, version 1.2.
-- Copyright (C) 2006-2014, Eagle Dynamics.
-- See http://www.lua.org for Lua script system info 
-- We recommend to use the LuaSocket addon (http://www.tecgraf.puc-rio.br/luasocket) 
-- to use standard network protocols in Lua scripts.
-- LuaSocket 2.0 files (*.dll and *.lua) are supplied in the Scripts/LuaSocket folder
-- and in the installation folder of the DCS. 

-- Expand the functionality of following functions for your external application needs.
-- Look into Saved Games\DCS\Logs\dcs.log for this script errors, please.

--[[	
-- Uncomment if using Vector class from the Scripts\Vector.lua file 
local lfs = require('lfs')
LUA_PATH = "?;?.lua;"..lfs.currentdir().."/Scripts/?.lua"
require 'Vector'
-- See the Scripts\Vector.lua file for Vector class details, please.
--]]

local default_output_file = nil

-- Defined socket for G940 led logic
local G940Socket = nil

-- Our Led state in a table
local LedState = { "0", "0", "0", "0", "0", "0", "0", "0" }

-- Define some constants, used for LED colors
local OFF = "0"
local GREEN = "g"
local AMBER = "a"
local RED = "r"

-- Store last found aircraft name
local aircraftNameLast = "NONE"

function LuaExportStart()
	-- Works once just before mission start.
	G940Socket = G940SocketSetup()
	G940Socket:send("hello=DCS World starting up!")
end

function LuaExportBeforeNextFrame()
	-- Works just before every simulation frame.
end

function LuaExportAfterNextFrame()
	-- Works just after every simulation frame.
end

function LuaExportStop()
	-- Works once just after mission stop.
	G940Socket:send("Leds=00000000;")
	G940Socket:send("bye=DCS World closing down!")
	G940Socket:close()
end

function LuaExportActivityNextEvent(t)
	local tNext = t
	local needLedUpdate = false
	
	-- Need to do a small test, because this function is called before actual exporting is started...
	if G940Socket ~= nil then
		-- Retrieve own aircraft name
		local acftName = "NONE"
		local selfData = LoGetSelfData()
		if selfData then
			acftName = selfData["Name"]
			if (aircraftNameLast ~= acftName) and (acftName ~= "") then
				aircraftNameLast = acftName
				G940Socket:send("hello=Found aircraft: "..acftName)
			end
		end
		
		-- Check for huey
		if acftName == "UH-1H" then
			ProcessHueyGameLogic(t)
			needLedUpdate = true
		end
		
		-- Check for A-10C
		if acftName == "A-10C" then
			ProcessA10CGameLogic(t)
			needLedUpdate = true
		end
		
		-- Check for Mirage 2000
		if acftName == "M-2000C" then
			ProcessM2000CGameLogic(t)
			needLedUpdate = true
		end
		
		-- Check for L-39C or L-39ZA
		if acftName == "L-39C" or acftName == "L-39ZA" then
			ProcessL39GameLogic(t)
			needLedUpdate = true
		end
		
		-- Check for AJS-37 Viggen
		if acftName == "AJS37" then
			ProcessAJS37GameLogic(t)
			needLedUpdate = true
		end
		
		-- Check for KA-50
		if acftName == "Ka-50" then
			ProcessKA50GameLogic(t)
			needLedUpdate = true
		end
		
		-- Check and run other aircraft code here, if you like
		
		-- If any module was detected, we update the led status
		if needLedUpdate then
			UpdateLeds(G940Socket, LedState)
			
			-- 0.1 second delay for faster led updates
			tNext = tNext + 0.1
			return tNext
		end
	end
	
	-- If we reach here, no known module was detected.
	-- We wait for 1 second before trying again.
	tNext = tNext + 1.0
	return tNext
end

-- Set all Leds to given state
function SetAllLed(state)
	for index = 1, 8 do
		LedState[index] = state
	end
end

-- This sets given led to given state, or OFF, depending on condition
function SetLedC(nr, state, condition)
	if condition then
		LedState[nr] = state
	end
end

-- This sets given led to given state
function SetLed(nr, state)
	LedState[nr] = state
end

-- This sends our current LedState to G940Leds server program (in a single packet)
function UpdateLeds(socket, state)
	socket:send("Leds="..table.concat(state)..";")
end

function G940SocketSetup()
	package.path  = package.path..";.\\LuaSocket\\?.lua"
	package.cpath = package.cpath..";.\\LuaSocket\\?.dll"
	socket = require("socket")
	address = "localhost"
	-- Default port for G940Leds is 33331
	port = "33331"
	
	-- Create new udp socket and setup destination
	udpsocket = socket.udp()
	udpsocket:setpeername(address, port)
	
	return udpsocket
end

function ProcessHueyGameLogic(t)
	local MainPanel = GetDevice(0)
	
	if MainPanel ~= 0 then
		-- Uncomment if you want the leds to update while outside of the cockpit
		-- MainPanel:update_arguments()
		
		-- Gather switch / lamp data
		local de_ice = MainPanel:get_argument_value(84) -- Switch 0 or 1
		local wpn_armed = MainPanel:get_argument_value(254) -- Lamp 0 or 0.9
		local wpn_safe = MainPanel:get_argument_value(255) -- Lamp 0 or 0.9
		local wpn_selector = MainPanel:get_argument_value(256) -- Switch -1 or 0
		local master_caution = MainPanel:get_argument_value(277) -- Lamp 0 or 1
		local flare_armed = MainPanel:get_argument_value(458) -- Lamp 0 or 1
		
		-- First, reset all to OFF
		SetAllLed(OFF)
		
		-- Now apply data to Leds
		SetLedC(1, GREEN, wpn_safe > 0.5)
		SetLedC(1, RED, wpn_armed > 0.5)
		SetLedC(3, GREEN, flare_armed > 0.5)
		SetLedC(4, GREEN, de_ice > 0.5)
		
		SetLedC(5, GREEN, wpn_safe > 0.5)
		SetLedC(5, RED, wpn_armed > 0.5)
		SetLedC(7, AMBER, flare_armed > 0.5)
		
		-- Some blink logic for master caution
		if master_caution > 0.5 then
			local int, fract = math.modf(t)
			if fract >= 0.5 then
				SetLed(8, RED)
			end
		end
		
		-- Special handling for 7.62mm and rocket selector:
		-- We only light buttons, if wpn_armed is set
		if wpn_armed > 0.5 then
			if wpn_selector >= 0 then
				SetLed(2, AMBER)
				SetLed(6, AMBER)
			else
				SetLed(2, GREEN)
				SetLed(6, GREEN)
			end
		end
	end
end

function ProcessA10CGameLogic(t)
	local MainPanel = GetDevice(0)
	
	if MainPanel ~= 0 then
		-- Uncomment if you want the leds to update while outside of the cockpit
		-- MainPanel:update_arguments()
		
		-- Gather switch / lamp data
		local autopilot = MainPanel:get_argument_value(131) -- Switch 0 or 1
		local gear_nose = MainPanel:get_argument_value(659) -- Lamp 1 if down. 0 if up or moving
		local gear_left = MainPanel:get_argument_value(660) -- Lamp 0 or 1
		local gear_right = MainPanel:get_argument_value(661) -- Lamp 0 or 1
		
		-- First, reset all to OFF
		SetAllLed(OFF)
		
		-- Now set all our TMS buttons to AMBER
		SetLed(3, AMBER)
		SetLed(6, AMBER)
		SetLed(7, AMBER)
		SetLed(8, AMBER)
		
		-- Now apply data to Leds
		SetLedC(1, GREEN, (gear_nose >= 1.0) and (gear_left >= 1.0) and (gear_right >= 1.0))
		SetLedC(4, GREEN, autopilot > 0.5)
	end
end

function ProcessM2000CGameLogic(t)
	local MainPanel = GetDevice(0)
	
	if MainPanel ~= 0 then
		-- Uncomment if you want the leds to update while outside of the cockpit
		-- MainPanel:update_arguments()
		
		-- Gather switch / lamp data
		local mirror = MainPanel:get_argument_value(183) -- Lamp 0 or 1
		local canopy = MainPanel:get_argument_value(656) -- Lamp 0 or 1
		local noseweel = MainPanel:get_argument_value(807) -- Lamp 0 or 1
		local parking_brake = MainPanel:get_argument_value(666) -- Lamp 0 or 1
		local land_gear = MainPanel:get_argument_value(404) -- Lamp 0 or 1
		local master_arm = MainPanel:get_argument_value(234) -- Lamp 0 or 1
		local master_warning = MainPanel:get_argument_value(191) -- Lamp 0 or 1
		local wpn_button1 = MainPanel:get_argument_value(250) -- Lamp 0 or 1
		local wpn_button2 = MainPanel:get_argument_value(253) -- Lamp 0 or 1
		local magic_standby = MainPanel:get_argument_value(272) -- Lamp 0 or 1
		local gun_button = MainPanel:get_argument_value(245) -- Lamp 0 or 1
        local gun_arm = MainPanel:get_argument_value(463) -- Lamp 0 or 1
        local warning = MainPanel:get_argument_value(658) -- Lamp 0 or 1
		local autonav = MainPanel:get_argument_value(667) -- Lamp 0 or 1
		
		-- First, reset all to OFF
		SetAllLed(OFF)
		
		-- Now apply data to Leds
		SetLedC(1, GREEN, parking_brake > 0.1)
		SetLedC(2, RED, land_gear > 0.1)
		SetLedC(4, AMBER, autonav > 0.1)
		SetLedC(5, AMBER, noseweel > 0.1)
		SetLedC(6, RED, mirror > 0.1)
		SetLedC(7, AMBER, gun_arm > 0.1)
		SetLedC(8, RED, master_arm > 0.1)
		
		-- Special handling for CANOPY
		if canopy > 0.1 then
				SetLed(3, RED)
			else
				SetLed(3, GREEN)
			
		end
		
	end
end

function ProcessL39GameLogic(t)
	local MainPanel = GetDevice(0)
	
	if MainPanel ~= 0 then
		-- Uncomment if you want the leds to update while outside of the cockpit
		-- MainPanel:update_arguments()
		
		-- Gather switch / lamp data
		--local flap0_cockpit1 = MainPanel:get_argument_value(278) -- Lamp 0 or 1
		local flap25_cockpit1 = MainPanel:get_argument_value(279) -- Lamp 0 or 1
		local flap44_cockpit1 = MainPanel:get_argument_value(280) -- Lamp 0 or 1
		--local flap0_cockpit2 = MainPanel:get_argument_value(462) -- Lamp 0 or 1
		local flap25_cockpit2 = MainPanel:get_argument_value(463) -- Lamp 0 or 1
		local flap44_cockpit2 = MainPanel:get_argument_value(464) -- Lamp 0 or 1
		local gear_cockpit1 = MainPanel:get_argument_value(113) -- Lamp 0 or 1
		--local gear_cockpit2 = MainPanel:get_argument_value(432) -- Lamp 0 or 1
		local canopy1 = MainPanel:get_argument_value(10) -- Lamp 0 or 1
		local canopy2 = MainPanel:get_argument_value(351) -- Lamp 0 or 1
		local master_caution1 = MainPanel:get_argument_value(253) -- Lamp 0 or 1
		local master_caution2 = MainPanel:get_argument_value(455) -- Lamp 0 or 1
		local danger_altitude1 = MainPanel:get_argument_value(2) -- Lamp 0 or 1
		local danger_altitude2 = MainPanel:get_argument_value(343) -- Lamp 0 or 1
		local air_brakes1 = MainPanel:get_argument_value(117) -- Lamp 0 or 1
		local air_brakes2 = MainPanel:get_argument_value(436) -- Lamp 0 or 1
		
		-- First, reset all to OFF
		SetAllLed(OFF)
		
		-- Now apply data to Leds
		SetLedC(1, RED, air_brakes1 > 0.3 or air_brakes2 > 0.3)
		SetLedC(2, RED, gear_cockpit1 > 0.3)
		SetLedC(3, RED, canopy1 > 0.3 or canopy2 > 0.3)
		SetLedC(4, AMBER, flap25_cockpit1 > 0.3 or flap25_cockpit2 > 0.3)
		SetLedC(5, RED)
		SetLedC(5, AMBER, danger_altitude1 > 0.3 or danger_altitude2 > 0.3)
		SetLedC(7, RED, master_caution1 > 0.3 or RED, master_caution2 > 0.3)
		SetLedC(8, AMBER, flap44_cockpit1 > 0.3 or flap44_cockpit2 > 0.3)
	end
end

function ProcessAJS37GameLogic(t)
	local MainPanel = GetDevice(0)
	
	if MainPanel ~= 0 then
		-- Uncomment if you want the leds to update while outside of the cockpit
		-- MainPanel:update_arguments()
		
		-- Gather switch / lamp data
		--TODO: I have no idea what "hojdl" means. Better name?
		local hojdl = MainPanel:get_argument_value(403) -- Lamp 0 or 1
		local attitude = MainPanel:get_argument_value(402) -- Lamp 0 or 1
		--TODO: I have no idea what "spakl" means. Better name?
		local spakl = MainPanel:get_argument_value(401) -- Lamp 0 or 1
		--local rpm10 = MainPanel:get_argument_value(140) -- Lamp 0 or 1
		--local rpm100 = MainPanel:get_argument_value(139) -- Lamp 0 or 1
		local altitude1000 = MainPanel:get_argument_value(113) -- Lamp 0 or 1
		--local altitude10000 = MainPanel:get_argument_value(114) -- Lamp 0 or 1
		local gear = MainPanel:get_argument_value(12) -- Lamp 0 or 1
		--local master_caution = MainPanel:get_argument_value(446) -- Lamp 0 or 1
		local canopy_closed = MainPanel:get_argument_value(9) -- Lamp 0 or 1
		local master_arm = MainPanel:get_argument_value(8) -- Lamp 0 or 1
		local engine_oil_pressure = MainPanel:get_argument_value(91)
		
		-- First, reset all to OFF
		SetAllLed(OFF)
		
		-- Now apply data to Leds
		SetLedC(1, GREEN, spakl > 0.1)
		SetLedC(2, GREEN, attitude > 0.1)
		SetLedC(3, GREEN, hojdl > 0.1)
		SetLedC(4, RED, gear > 0.1)
		SetLedC(5, GREEN, canopy_closed > 0.1)
		SetLedC(6, RED, engine_oil_pressure > 0.1)
		SetLedC(7, AMBER, altitude1000 > 0.1)
		SetLedC(8, RED, master_arm > 0.1)
	end
end

function ProcessKA50GameLogic(t)
	local MainPanel = GetDevice(0)
	
	if MainPanel ~= 0 then
		-- Uncomment if you want the leds to update while outside of the cockpit
		-- MainPanel:update_arguments()
	
		-- Lit button returns 0.1 when active. 0.2 when depressed but not active. 0.3 when depressed and active
		-- Indicators returns between 0.0 to 1.0, mostly only 0.0 and 1.0 but for example gear_handle moves between them
		-- 		Indicators with continous values are marked as such)
	
		-- Auto-pilot buttons and switches
		local bankhold = MainPanel:get_argument_value(330) -- Lit button
		local pitchhold = MainPanel:get_argument_value(331) -- Lit button
		local headinghold = MainPanel:get_argument_value(332) -- Lit button
		local altitudehold = MainPanel:get_argument_value(333) -- Lit button
		--local flightdirector = MainPanel:get_argument_value(334) -- Lit button
		--local ap_headingtrack = MainPanel:get_argument_value(336) -- 0.0 heading, 0.5 nothing (hold current course), 1.0 for track
		--local ap_baroralt = MainPanel:get_argument_value(335) -- 0.0 barometric, 0.5 nothing, 1.0 radar
		
		-- Target mode
		local autoturn = MainPanel:get_argument_value(437) -- Lit button
		local airborne = MainPanel:get_argument_value(438) -- Lit button
		local forwardhemisphere = MainPanel:get_argument_value(439) -- Lit button
		local groundmoving = MainPanel:get_argument_value(440) -- Lit button
		
		-- Pushable warning lights
		local master_caution = MainPanel:get_argument_value(44) -- Indicator
		local engine_rpm_warning = MainPanel:get_argument_value(46) -- Indicator
		
		-- Misc
		--local gear_handle = MainPanel:get_argument_value(65) -- Indicator (continous values)
		--local navigation_lights = MainPanel:get_argument_value(146) -- 0.0 off, 0.1 10%, 0.2 50%, 0.3 100%
		--local hover_lamp = MainPanel:get_argument_value(175) -- Indicator
		--local ralt_lamp = MainPanel:get_argument_value(170) -- Indicator
		
		-- Weapons switches
		--local master_arm = MainPanel:get_argument_value(387) -- Indicator
		--local rate_of_fire = MainPanel:get_argument_value(398) -- Indicator
		--local cannon_round = MainPanel:get_argument_value(399) -- Indicator
		--local burst_length = MainPanel:get_argument_value(400) -- 0.0 for short. 0.1 for medium, 0.2 for long
		--local manualautomode = MainPanel:get_argument_value(403) -- Indicator 1.0 for auto, 0.0 for manual
		--local trainingmode = MainPanel:get_argument_value(432) -- Indicator 1.0 for traning mode, 0.0 for manual
		--local tracking_gunsight = MainPanel:get_argument_value(436) -- Indicator 1.0 for tracking, 0.0 for gunsight
		--local laser_standby = MainPanel:get_argument_value(435) -- Indicator
		
		-- PVI
		--local nav_waypoints = MainPanel:get_argument_value(315) -- Lit button
		--local nav_fixpoints = MainPanel:get_argument_value(316) -- Lit button
		--local nav_airfields = MainPanel:get_argument_value(317) -- Lit button
		--local nav_targets = MainPanel:get_argument_value(318) -- Lit button
		--local nav_initialnavpos = MainPanel:get_argument_value(522) -- Lit button
		--local nav_selfpos = MainPanel:get_argument_value(319) -- Lit button
		--local nav_course = MainPanel:get_argument_value(320) -- Lit button
		--local nav_wind = MainPanel:get_argument_value(321) -- Lit button
		--local nav_trueheading = MainPanel:get_argument_value(322) -- Lit button
		
		-- Datalink
		--local dlink_toall = MainPanel:get_argument_value(16) -- Lit button
		--local dlink_wingman1 = MainPanel:get_argument_value(17) -- Lit button
		--local dlink_wingman2 = MainPanel:get_argument_value(18) -- Lit button
		--local dlink_wingman3 = MainPanel:get_argument_value(19) -- Lit button
		--local dlink_wingman4 = MainPanel:get_argument_value(20) -- Lit button
		--local dlink_vehicle = MainPanel:get_argument_value(21) -- Lit button
		--local dlink_sam = MainPanel:get_argument_value(22) -- Lit button
		--local dlink_other = MainPanel:get_argument_value(23) -- Lit buttons
		--local dlink_ingress = MainPanel:get_argument_value(50) -- Lit buttons
		--local dlink_send = MainPanel:get_argument_value(159) -- Lit buttons
		--local dlink_ingresstotarget = MainPanel:get_argument_value(150) -- Lit buttons
		--local dlink_erase = MainPanel:get_argument_value(161) -- Lit buttons
		
		
		-- Gear  (0.0 not, 1.0 yes)
		--local lg_up = MainPanel:get_argument_value(59)
		--local lg_down = MainPanel:get_argument_value(60)
		--local rg_up = MainPanel:get_argument_value(61)
		--local rg_down = MainPanel:get_argument_value(62)
		--local ng_up = MainPanel:get_argument_value(63)
		--local ng_down = MainPanel:get_argument_value(64)
		
		-- First, reset all to OFF
		SetAllLed(OFF)
		
		-- Now apply data to Leds
		SetLedC(3, GREEN, bankhold > 0.1)
		SetLedC(4, GREEN, pitchhold > 0.1)
		SetLedC(7, GREEN, headinghold > 0.1)
		SetLedC(8, GREEN, altitudehold > 0.1)
		
		SetLedC(1, AMBER, autoturn > 0.1)
		SetLedC(2, AMBER, forwardhemisphere > 0.1)
		SetLedC(5, AMBER, airborne > 0.1)
		SetLedC(6, AMBER, groundmoving > 0.1)
				
		-- Some blink logic for master caution
		if master_caution > 0.0 then
			local int, fract = math.modf(t)
			if fract >= 0.0 then
				SetLed(8, RED)
			end
		end
		
		-- Some blink logic for master caution
		if engine_rpm_warning > 0.0 then
			local int, fract = math.modf(t)
			if fract >= 0.0 then
				SetLed(7, RED)
			end
		end	
	end
end

