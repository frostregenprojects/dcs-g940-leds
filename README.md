Welcome to DCS G940 LED's Export.lua script collection!

[[_TOC_]]

# Installation
1. Download latest artifacts.zip from https://gitlab.com/frostregenprojects/dcs-g940-leds/-/tags (Dropdown "package_zip")
1. Extract G940leds.exe to any place
2. Extract Export.lua into "C:\Users\\%UserProfile%\Saved Games\DCS\Scripts"

# Playing
1. Run G940leds.exe
2. Start DCS and play

# Information
## Credits
This is based on the works from other people, credits:
- Fantastic G940leds.exe done by Martin Larsson (nickname morg at forums.eagle.ru and morg@borgeby.se)
- "PVI_Eagle" for adding support for Ka-50, M-2000C, A-10C, L-39C, L-39ZA, AJS37

## Sources
- G940Leds forum thread: https://forums.eagle.ru/forum/english/dcs-world-topics/input-and-output/44004-g940-leds-working-program?t=45895&highlight=G940
- PVI_Eagle forums thread: https://forums.eagle.ru/forum/english/dcs-world-topics/input-and-output/153994-export-data-to-from-dcsw-fc3-to-udp-socket?postcount=5#post4466553
- DCS World: https://www.digitalcombatsimulator.com

## How it started
It originally came with an implementation for the DCS A10-C aircraft,
and I started to implement an LED scheme for the DCS UH-1H Huey helicopter.

While doing this, I tried to simplify installation into one Export.lua "drop in",
as well as improve the performance.
Currently it supports easy extension for more aircraft, and only sends one UDP network
message each frame (instead of multiple ones from original implementation).

## Where it should go
I decided to make this public on gitlab with the hope to integrate other LED setups for all the other DCS modules.
So please send me merge requests for your control schemes for different aircraft.

All those setups are intended to be good defaults, but since everyone has slightly different taste and key bindings,
you might want to adjust it to your own liking.

Greetings,
Frostregen

